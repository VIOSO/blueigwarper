/////////////////////////////////////
/////////////////////////////////////
/////////////////////////////////////
//WARNING
//This file has been autogenerated.
//Do NOT make modifications directly to it as they will be overwritten!
/////////////////////////////////////
/////////////////////////////////////
/////////////////////////////////////

#ifndef VIOSOWARPER_PROXY_H
#define VIOSOWARPER_PROXY_H

#include <ComponentAPI.h>
#include <DX11RenderingInterfaceListenerAPI.h>
#include <IGCameraAspectAPI.h>

GEARS_EXPORT void GEARS_API RegisterAPI_v6(APIManager_RegisterAPI_Func_v6 register_api);

#endif
